<?php require_once 'inc/top.php';?>

<?php

if (!isset($_SESSION['kori'])) {
  $_SESSION['kori'] = array();
}


if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $kori = $_SESSION['kori'];
  $tuote = filter_input(INPUT_POST,'tuote',FILTER_SANITIZE_STRING);
  array_push($kori,$tuote);
  $_SESSION['kori'] = $kori;
}
?>
<div class="row">
  <div class="col" style="text-align: right;">
    <?php
    print "<div class='float-right'><a href='ostoskori.php'><p id='ostoskori'>" . count($_SESSION['kori']) . "</p></a></div>";
    ?>
  </div>
</div>
<?php
$sql = 'select * from tuote';

$tuoteryhma_id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
if ($tuoteryhma_id) {
  $sql = $sql . ' where tuoteryhma_id = ' . $tuoteryhma_id; 
}

$sql = $sql . ' order by nimi';

//print "Valitsit tuoteryhmän " . $tuoteryhma_id;

try {
  
  $kysely = $tietokanta->query($sql);
  while ($tietue = $kysely->fetch()) {
    
    print "<form action='" . $_SERVER['PHP_SELF'] . "' method='post'>";
    print "<input name='tuote' type='hidden' value='" . $tietue['id'] . "'>";
    print "<div class='mb-5'>";
    print "<img src='img/" . $tietue['kuva'] . "'>";
    print "<h3>" . $tietue['nimi'] . "</h3>";
    print "<p>" . $tietue['kuvaus'] .  "</p>";
    print "<p>" . $tietue['hinta'] . "</p>";
    print "<button class='btn btn-primary'>Osta</button>";
    print "</div>";
    print "</form>";

  }
} catch (PDOException $pdoex) {
  print $pdoex->getMessage();
}
?>

<!--
<form action="<?php //print($_SERVER['PHP_SELF']);?>" method="post">
<input name="tuote" type="hidden" value="Halo Down Hoodie, miesten untuvatakki">
<div class="mb-5">
  <img src="img/takki.png">
  <h3>The North Face</h3>
  <p>
  Halo Down Hoodie, miesten untuvatakki
  </p>
  <p>
  70 €
  </p>
  <button class="btn btn-primary">Osta</button>
</div>
</form>

<form action="<?php //print($_SERVER['PHP_SELF']);?>" method="post">
<input name="tuote" type="hidden" value="Vision II GTX, miesten retkeilykengät">
<div>
  <img src="img/kengat.png">
  <h3>Haglöfs</h3>
  <p>
  Vision II GTX, miesten retkeilykengät
  </p>
  <p>
  60 €
  </p>
</div>
<button class="btn btn-primary">Osta</button>
</form>
-->
<?php require_once 'inc/bottom.php';?>