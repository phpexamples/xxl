<?php
session_start();
session_regenerate_id();

try {
  $tietokanta = new PDO('mysql:host=localhost;dbname=verkkokauppa;charset=utf8','root','root1234');
  $tietokanta->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $pdoex) {
  print $pdoex->getMessage();
  exit;
}

?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/style.css">
    <title>Verkkokauppa</title>
  </head>
  <body>
  <header>
      <!-- Fixed navbar -->
      <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Verkkokauppa</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
          <ul class="navbar-nav mr-auto">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Tuoteryhmät
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <?php
          try {
            $sql = 'select * from tuoteryhma order by nimi';
            $kysely = $tietokanta->query($sql);
            while ($tietue = $kysely->fetch()) {
              print "<a class='dropdown-item active' href='" . $_SERVER['PHP_SELF'] . "?id=" . $tietue['id'] ."'>" . $tietue['nimi'] . "</a>";
            }

          } catch (PDOException $pdoex) {
            print $pdoex->getMessage();
          }
          ?>          

          <!--
          <a class="dropdown-item" href="#">Kengät</a>
          <a class="dropdown-item" href="#">Takit</a>
          -->
        </div>
      </li>
      </ul>
      
        </div> 
      </nav>
    </header>
  <div class="container">
