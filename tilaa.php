<?php require_once 'inc/top.php'?>
<?php
$etunimi = filter_input(INPUT_POST,'etunimi',FILTER_SANITIZE_STRING);
$sukunimi = filter_input(INPUT_POST,'sukunimi',FILTER_SANITIZE_STRING);

try {
  $tietokanta->beginTransaction();
  $sql = "insert into asiakas (etunimi, sukunimi) values (:etunimi, :sukunimi)";
  $kysely = $tietokanta->prepare($sql);
  $kysely->bindValue(':etunimi',$etunimi,PDO::PARAM_STR);
  $kysely->bindValue(':sukunimi',$sukunimi,PDO::PARAM_STR);
  $kysely->execute();

  $asiakas_id = $tietokanta->lastInsertId();

  $sql = "insert into tilaus (tila, asiakas_id) values (:tila, :asiakas_id)";
  $tilauskysely = $tietokanta->prepare($sql);
  $tilauskysely->bindValue(':tila','tilattu',PDO::PARAM_STR);
  $tilauskysely->bindValue(':asiakas_id',$asiakas_id,PDO::PARAM_STR);
  $tilauskysely->execute();

  $tilaus_id = $tietokanta->lastInsertId();
  $kori = $_SESSION['kori'];

  //var_dump($kori);

  foreach ($kori as $tuote_id) {
    $sql = "insert into tilausrivi (tilaus_id, tuote_id) values (:tilaus_id, :tuote_id)";
    $tilauskysely = $tietokanta->prepare($sql);
    $tilauskysely->bindValue(':tilaus_id',$tilaus_id,PDO::PARAM_STR);
    $tilauskysely->bindValue(':tuote_id',$tuote_id,PDO::PARAM_STR);
    $tilauskysely->execute();
  }
  $_SESSION['kori'] = null;

  print "<p>Tilaus toimitetaan!</p>";
  $tietokanta->commit();

}
catch (PDOException $pdoex) {
  $tietokanta->rollback();
  print $pdoex->getMessage();
}
?>
<?php require_once 'inc/bottom.php'?>