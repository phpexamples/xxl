<?php 
require_once 'inc/top.php'; 
?>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  $_SESSION['kori'] = null;
}
?>
<h3>Ostoskori</h3>
<ul>
<?php
$kori = $_SESSION['kori'];
if ($kori) {
  $summa = 0;
  foreach ($kori as $id) {
    $sql ="select * from tuote where id = $id";
    $kysely = $tietokanta->query($sql);
    $ostos = $kysely->fetch();
    $summa = $summa + $ostos['hinta'];
    print "<li>" . $ostos['nimi'] . ', ' .  $ostos['hinta'] .  " €</li>";
  }
  print "<p>Yhteensä $summa €</p>";
}
?>
</ul>
<form action="<?php $_SERVER['PHP_SELF'];?>" method="post">
<button class="btn btn-primary">Tyhjennä</button>
<a href="index.php">Takaisin kauppaan</a>
</form>
<h3 style='margin-top: 30px;'>Tilaa</h3>
<form action="tilaa.php" method="post">
  <div class="form-group">
    <label>Etunimi</label>
    <input name="etunimi" class="form-control">
  </div>
  <div class="form-group">
    <label>Sukunimi</label>
    <input name="sukunimi" class="form-control">
  </div>
  <button>Tilaa</button>
</form>
<?php require_once 'inc/bottom.php'; ?>